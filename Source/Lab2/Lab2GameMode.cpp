// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lab2GameMode.h"
#include "Lab2HUD.h"
#include "Lab2Character.h"
#include "UObject/ConstructorHelpers.h"

ALab2GameMode::ALab2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ALab2HUD::StaticClass();
}
